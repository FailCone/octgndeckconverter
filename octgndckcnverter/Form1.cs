﻿//Form1.cs - Created March 2, 2010
//Last Updated: August 17, 2012
//Form for the entire OCTGN Deck Converter program. 

//Ryan note (July 2012): Oh god what have I done here? This is a mess.
//Getting it to work first, then cleaning it up afterwards.

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        const int mainDeck = 1;
        const int sideboard = 2;
        const int commandZone = 3;
        bool willSaveTXT;
        public Form1()
        {
            InitializeComponent();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            ofdOpenFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ofdOpenFile.InitialDirectory += "\\Octgn\\Database\\a6c8d2e8-7cd8-11dd-8f94-e62b56d89593\\Decks";
            ofdOpenFile.Filter = "O8D File|*.o8d";
            DialogResult result = ofdOpenFile.ShowDialog();
            if (result == DialogResult.OK)
            {
                willSaveTXT = true;
                deckList.Items.Clear();
                cardLink.Links.Clear();
                cardLink.Text = "None";

                txtBox.Text = null;
                string ofileName = ofdOpenFile.FileName;
                fileBox.Text = ofileName;

                System.IO.StreamReader insStream = new System.IO.StreamReader(@ofileName);
                string insLine = insStream.ReadLine();
                insLine = insStream.ReadLine();
                insLine = insStream.ReadLine();
                insLine = insStream.ReadLine();
                while (insLine != null && insLine != "  </section>")
                {
                    //Format and build list items
                    lineToList(insLine, mainDeck);
                    insLine = insStream.ReadLine();
                }
                insLine = insStream.ReadLine();
                if (insLine == "  <section name=\"Sideboard\">")
                {
                    insLine = insStream.ReadLine();
                    while (insLine != null && insLine != "  </section>")
                    {
                        lineToList(insLine, sideboard);
                        insLine = insStream.ReadLine();
                    }
                    insLine = insStream.ReadLine();
                }
                if (insLine == "  <section name=\"Command Zone\">")
                {
                    insLine = insStream.ReadLine();
                    while (insLine != null && insLine != "  </section>")
                    {
                        lineToList(insLine, commandZone);
                        insLine = insStream.ReadLine();
                    }
                    insLine = insStream.ReadLine();
                }
                insStream.Close();
                insStream.Dispose();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //Clear List Butan1
        //Butan push
        private void button2_Click(object sender, EventArgs e)
        {
            fileBox.Text = "";
            txtBox.Text = "";
            deckList.Items.Clear();
            sideboardList.Items.Clear();
            czList.Items.Clear();
            cardLink.Links.Clear();
            cardLink.Text = "None";
        }
        //Save File Butan
        private void saveButton_Click(object sender, EventArgs e)
        {
            sfdSaveFile.FileName = null;
            if (willSaveTXT)
            {
                sfdSaveFile.Filter = "Text File|.txt";
                sfdSaveFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }
            else
            {
                sfdSaveFile.Filter = "OCTGN Deck File|.o8d";
                sfdSaveFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                sfdSaveFile.InitialDirectory += "\\Octgn\\Database\\a6c8d2e8-7cd8-11dd-8f94-e62b56d89593\\Decks";
            }
            sfdSaveFile.ShowDialog();
        }

        private void sfdSaveFile_FileOk(object sender, CancelEventArgs e)
        {
            string outfileName = sfdSaveFile.FileName;

            System.IO.StreamWriter outFile = new System.IO.StreamWriter(@outfileName);
            if (!willSaveTXT)
            {
                string header = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>";
                outFile.WriteLine(header);
                header = "<deck game=\"a6c8d2e8-7cd8-11dd-8f94-e62b56d89593\">";
                outFile.WriteLine(header);
                header = "  <section name=\"Main\">";
                outFile.WriteLine(header);
            }
            else
            {
                outFile.WriteLine("----MAIN DECK----");
            }
            for (int i = 0; i < deckList.Items.Count; i++)
            {
                string tempStr = "";
                if (willSaveTXT)
                {
                    tempStr = tempStr + deckList.Items[i].SubItems[1].Text;
                    tempStr = tempStr + ' ';
                    tempStr = tempStr + deckList.Items[i].SubItems[0].Text;
                } else { 
                    tempStr = "    <card qty=\"";
                    tempStr += deckList.Items[i].SubItems[1].Text;
                    tempStr += "\" >";
                    tempStr += deckList.Items[i].SubItems[0].Text;
                    tempStr += "</card>";
                }
                outFile.WriteLine(tempStr);
            }
            if (!willSaveTXT)
            {
                outFile.WriteLine("  </section>");
            }
            else
            {
                outFile.WriteLine("\n");
            }
            if (sideboardList.Items.Count > 0)
            {
                if (!willSaveTXT)
                {
                    outFile.WriteLine("  <section name=\"Sideboard\">");
                }
                else
                {
                    outFile.WriteLine("----SIDEBOARD----");
                }
                for (int i = 0; i < sideboardList.Items.Count; i++)
                {
                    string tempStr = "";
                    if (willSaveTXT)
                    {
                        tempStr = tempStr + sideboardList.Items[i].SubItems[1].Text;
                        tempStr = tempStr + ' ';
                        tempStr = tempStr + sideboardList.Items[i].SubItems[0].Text;
                    }
                    else
                    {
                        tempStr = "    <card qty=\"";
                        tempStr += sideboardList.Items[i].SubItems[1].Text;
                        tempStr += "\" >";
                        tempStr += sideboardList.Items[i].SubItems[0].Text;
                        tempStr += "</card>";
                    }
                    outFile.WriteLine(tempStr);
                }
                if (!willSaveTXT)
                {
                    outFile.WriteLine("  </section>");
                }
                else
                {
                    outFile.WriteLine("\n");
                }
            }

            if (czList.Items.Count > 0)
            {
                if (!willSaveTXT)
                {
                    outFile.WriteLine("  <section name=\"Command Zone\">");
                }
                else
                {
                    outFile.WriteLine("----COMMAND ZONE----");
                }
                for (int i = 0; i < czList.Items.Count; i++)
                {
                    string tempStr = "";
                    if (willSaveTXT)
                    {
                        tempStr = tempStr + czList.Items[i].SubItems[1].Text;
                        tempStr = tempStr + ' ';
                        tempStr = tempStr + czList.Items[i].SubItems[0].Text;
                    }
                    else
                    {
                        tempStr = "    <card qty=\"";
                        tempStr += czList.Items[i].SubItems[1].Text;
                        tempStr += "\" >";
                        tempStr += czList.Items[i].SubItems[0].Text;
                        tempStr += "</card>";
                    }
                    outFile.WriteLine(tempStr);
                }
                if (!willSaveTXT)
                {
                    outFile.WriteLine("  </section>");
                }
                else
                {
                    outFile.WriteLine("\n");
                }
            }

            if (!willSaveTXT)
            {
                outFile.WriteLine("</deck>");
            }
            outFile.Close();
            outFile.Dispose();
        }

        private void deckList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            string curItem = deckList.Items[e.Item.Index].Text;
            string linkLocation = "www.magiccards.info/query?q=";
            linkLocation = linkLocation + curItem;
            cardLink.Text = curItem;
            cardLink.Links.Clear();
            cardLink.Links.Add(0, curItem.Length, linkLocation);
        }

        private void cardLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.cardLink.Links[0].Visited = true;

            string target = e.Link.LinkData.ToString();

            if(target != null && target.StartsWith("www"))
            {
                System.Diagnostics.Process.Start(target);
            }

            else
            {
                MessageBox.Show("No Card Selected");
            }
        }

        private void txtConvertBtn_Click(object sender, EventArgs e)
        {
            ofdOpenFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            ofdOpenFile.Filter = "Text File|*.txt";
            DialogResult result = ofdOpenFile.ShowDialog();
            if (result == DialogResult.OK)
            {
                
                //If the file is empty, do nothing.
                if (new FileInfo(ofdOpenFile.FileName).Length == 0)
                {
                    System.Windows.Forms.MessageBox.Show("File is empty.");
                    return;
                }
                
                
                willSaveTXT = false;
                deckList.Items.Clear();
                cardLink.Links.Clear();
                cardLink.Text = "None";

                fileBox.Text = null;
                string ofileName = ofdOpenFile.FileName;
                txtBox.Text = ofileName;

                System.IO.StreamReader insStream = new System.IO.StreamReader(@ofileName);
                string insLine = insStream.ReadLine();
                System.Console.Write(insLine);

                while (insLine != "" && insLine != null)
                {
                    textToList(insLine, mainDeck);
                       
                    insLine = insStream.ReadLine();
                }
                insStream.Close();
                insStream.Dispose();
            }
        }
            

        public void lineToList(string insLine, int where)
        {
            string cardQty = "";
            string cardName = "";
            bool foundQty = false;
            bool foundName = false;
            for (int i = 0; !foundQty || !foundName; i++)
            {
                if (insLine[i] == '"' && !foundQty)
                {
                    i++;
                    while (insLine[i] != '"')
                    {
                        cardQty = cardQty + insLine[i];
                        i++;
                    }
                    foundQty = true;


                }
                if (insLine[i] == '>' && !foundName)
                {
                    i++;
                    while (insLine[i] != '<')
                    {
                        cardName = cardName + insLine[i];
                        i++;
                    }
                    foundName = true;
                }
            }
            ListViewItem objListItem;
            if (where == mainDeck)
            {
                objListItem = deckList.Items.Add(cardName);
            }
            else if (where == sideboard)
            {
                objListItem = sideboardList.Items.Add(cardName);
            }
            else
            {
                objListItem = czList.Items.Add(cardName);
                
            }
            objListItem.SubItems.Add(cardQty);
        }

        public void textToList(string insLine, int where)
        {
            if (char.IsDigit(insLine[0]))
            {
                //Format and build list items
                string cardQty = "";
                string cardName = "";
                bool foundQty = false;
                //GET QUANTITY AND NAME
                int i = 0;
                while (!foundQty)
                {
                    if (Char.IsDigit(insLine[i]))
                    {
                        cardQty += insLine[i];
                        i++;
                    }
                    else
                    {
                        if (cardQty != "")
                        {
                            foundQty = true;
                        }
                        else
                        {
                            i++;

                        }
                    }
                }
                bool leadingSpace = false;
                for (; i < insLine.Length; i++)
                {

                    if (insLine[i] != ' ')
                    {
                        leadingSpace = true;
                    }
                    if (leadingSpace == true)
                    {
                        cardName += insLine[i];
                    }
                }
                ListViewItem objListItem;
                if (where == mainDeck)
                {
                    objListItem = deckList.Items.Add(cardName);
                }
                else if (where == sideboard)
                {
                    objListItem = sideboardList.Items.Add(cardName);
                }
                else
                {
                    objListItem = czList.Items.Add(cardName);

                }
                objListItem.SubItems.Add(cardQty);
            }

        }
        public void pasteToList(int where)
        {
            if (!Clipboard.ContainsText())
            {
                MessageBox.Show("Only text goes here!");
            }
            string fromClipboard = Clipboard.GetText();


            using (StringReader reader = new StringReader(fromClipboard))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    textToList(line, where);
                }
            }

        }

        private void clearAll_Click(object sender, EventArgs e)
        {
            deckList.Items.Clear();
            sideboardList.Items.Clear();
            czList.Items.Clear();
        }

        private void sideboardList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            string curItem = sideboardList.Items[e.Item.Index].Text;
            string linkLocation = "www.magiccards.info/query?q=";
            linkLocation = linkLocation + curItem;
            cardLink.Text = curItem;
            cardLink.Links.Clear();
            cardLink.Links.Add(0, curItem.Length, linkLocation);
        }

        private void czList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            string curItem = czList.Items[e.Item.Index].Text;
            string linkLocation = "www.magiccards.info/query?q=";
            linkLocation = linkLocation + curItem;
            cardLink.Text = curItem;
            cardLink.Links.Clear();
            cardLink.Links.Add(0, curItem.Length, linkLocation);
        }

        private void decklistPaste_Click(object sender, EventArgs e)
        {
            pasteToList(mainDeck);
            willSaveTXT = false;
        }

        private void czPaste_Click(object sender, EventArgs e)
        {
            pasteToList(commandZone);
        }

        private void sideboardPaste_Click(object sender, EventArgs e)
        {
            pasteToList(sideboard);
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }


    }
}

 
