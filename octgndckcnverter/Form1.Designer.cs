﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ofdOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.deckList = new System.Windows.Forms.ListView();
            this.cardName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cardQty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fileBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.sfdSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.cardLink = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.txtConvertBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.sideboardList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.czList = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.decklistPaste = new System.Windows.Forms.Button();
            this.sideboardPaste = new System.Windows.Forms.Button();
            this.czPaste = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // deckList
            // 
            this.deckList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cardName,
            this.cardQty});
            this.deckList.Location = new System.Drawing.Point(12, 19);
            this.deckList.Name = "deckList";
            this.deckList.Size = new System.Drawing.Size(234, 338);
            this.deckList.TabIndex = 0;
            this.deckList.UseCompatibleStateImageBehavior = false;
            this.deckList.View = System.Windows.Forms.View.Details;
            this.deckList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.deckList_ItemSelectionChanged);
            // 
            // cardName
            // 
            this.cardName.Text = "Card Name";
            this.cardName.Width = 180;
            // 
            // cardQty
            // 
            this.cardQty.Text = "Qty.";
            this.cardQty.Width = 32;
            // 
            // fileBox
            // 
            this.fileBox.Location = new System.Drawing.Point(495, 45);
            this.fileBox.Name = "fileBox";
            this.fileBox.Size = new System.Drawing.Size(254, 20);
            this.fileBox.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(495, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(254, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "Open .o8d and convert to .txt\r\n";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(495, 352);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 34);
            this.button2.TabIndex = 3;
            this.button2.Text = "Clear Lists";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.clearAll_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(674, 267);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 34);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save Contents";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(674, 354);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 34);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // sfdSaveFile
            // 
            this.sfdSaveFile.FileOk += new System.ComponentModel.CancelEventHandler(this.sfdSaveFile_FileOk);
            // 
            // cardLink
            // 
            this.cardLink.AutoSize = true;
            this.cardLink.Location = new System.Drawing.Point(541, 9);
            this.cardLink.Name = "cardLink";
            this.cardLink.Size = new System.Drawing.Size(33, 13);
            this.cardLink.TabIndex = 6;
            this.cardLink.TabStop = true;
            this.cardLink.Text = "None";
            this.cardLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.cardLink_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(492, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Selected:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(492, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "OCTGN deck to text file:";
            // 
            // txtBox
            // 
            this.txtBox.Location = new System.Drawing.Point(495, 124);
            this.txtBox.Name = "txtBox";
            this.txtBox.Size = new System.Drawing.Size(254, 20);
            this.txtBox.TabIndex = 8;
            // 
            // txtConvertBtn
            // 
            this.txtConvertBtn.Location = new System.Drawing.Point(495, 150);
            this.txtConvertBtn.Name = "txtConvertBtn";
            this.txtConvertBtn.Size = new System.Drawing.Size(254, 35);
            this.txtConvertBtn.TabIndex = 9;
            this.txtConvertBtn.Text = "Open .txt and convert to .o8d";
            this.txtConvertBtn.UseVisualStyleBackColor = true;
            this.txtConvertBtn.Click += new System.EventHandler(this.txtConvertBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(492, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Text file to OCTGN deck";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(492, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Decks should be formatted like this:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(512, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "4 Goblin Game";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(512, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "2 Vexing Devil";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(512, 227);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "15 Mountain";
            // 
            // sideboardList
            // 
            this.sideboardList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.sideboardList.Location = new System.Drawing.Point(252, 19);
            this.sideboardList.Name = "sideboardList";
            this.sideboardList.Size = new System.Drawing.Size(234, 216);
            this.sideboardList.TabIndex = 16;
            this.sideboardList.UseCompatibleStateImageBehavior = false;
            this.sideboardList.View = System.Windows.Forms.View.Details;
            this.sideboardList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.sideboardList_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Card Name";
            this.columnHeader1.Width = 182;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Qty.";
            this.columnHeader2.Width = 31;
            // 
            // czList
            // 
            this.czList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.czList.Location = new System.Drawing.Point(252, 283);
            this.czList.Name = "czList";
            this.czList.Size = new System.Drawing.Size(234, 74);
            this.czList.TabIndex = 17;
            this.czList.UseCompatibleStateImageBehavior = false;
            this.czList.View = System.Windows.Forms.View.Details;
            this.czList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.czList_ItemSelectionChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Card Name";
            this.columnHeader3.Width = 180;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Qty.";
            this.columnHeader4.Width = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Main Deck:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(249, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Sideboard:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(255, 267);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Command Zone:";
            // 
            // decklistPaste
            // 
            this.decklistPaste.Location = new System.Drawing.Point(12, 365);
            this.decklistPaste.Name = "decklistPaste";
            this.decklistPaste.Size = new System.Drawing.Size(117, 23);
            this.decklistPaste.TabIndex = 21;
            this.decklistPaste.Text = "Paste From Clipboard";
            this.decklistPaste.UseVisualStyleBackColor = true;
            this.decklistPaste.Click += new System.EventHandler(this.decklistPaste_Click);
            // 
            // sideboardPaste
            // 
            this.sideboardPaste.Location = new System.Drawing.Point(252, 241);
            this.sideboardPaste.Name = "sideboardPaste";
            this.sideboardPaste.Size = new System.Drawing.Size(117, 23);
            this.sideboardPaste.TabIndex = 22;
            this.sideboardPaste.Text = "Paste From Clipboard";
            this.sideboardPaste.UseVisualStyleBackColor = true;
            this.sideboardPaste.Click += new System.EventHandler(this.sideboardPaste_Click);
            // 
            // czPaste
            // 
            this.czPaste.Location = new System.Drawing.Point(252, 365);
            this.czPaste.Name = "czPaste";
            this.czPaste.Size = new System.Drawing.Size(117, 23);
            this.czPaste.TabIndex = 23;
            this.czPaste.Text = "Paste From Clipboard";
            this.czPaste.UseVisualStyleBackColor = true;
            this.czPaste.Click += new System.EventHandler(this.czPaste_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 400);
            this.Controls.Add(this.czPaste);
            this.Controls.Add(this.sideboardPaste);
            this.Controls.Add(this.decklistPaste);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.czList);
            this.Controls.Add(this.sideboardList);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtConvertBtn);
            this.Controls.Add(this.txtBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cardLink);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.fileBox);
            this.Controls.Add(this.deckList);
            this.Name = "Form1";
            this.Text = "OCTGN Deck Converter (ALPHA) v.02";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog ofdOpenFile;
        private System.Windows.Forms.ListView deckList;
        private System.Windows.Forms.ColumnHeader cardName;
        private System.Windows.Forms.ColumnHeader cardQty;
        private System.Windows.Forms.TextBox fileBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.SaveFileDialog sfdSaveFile;
        private System.Windows.Forms.LinkLabel cardLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBox;
        private System.Windows.Forms.Button txtConvertBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListView sideboardList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView czList;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button decklistPaste;
        private System.Windows.Forms.Button sideboardPaste;
        private System.Windows.Forms.Button czPaste;
    }
}

